#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/device.h>         
#include <linux/module.h>         
#include <linux/kernel.h>         
#include <linux/fs.h>             
#include <asm/uaccess.h>          
#define  DEVICE_NAME "S3436413Device"    
#define  CLASS_NAME  "charDriver"        

//Module will not insert without license.
MODULE_LICENSE("GPL");    

static int majorNumber;                  
static int numberOpens = 0;       
static struct class*  charDriverClass  = NULL; 
static struct device* charDriverDevice = NULL;

static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);
static int dev_open(struct inode *, struct file *);
static int dev_close(struct inode *, struct file *);

//The device to be driven.
struct virtual_device {
  char data[100];
  int size_of_data;
  struct cdev cdev;
  struct semaphore sem;
}myDevice;

//Defining file operations for the driver.
static struct file_operations fops = {
  .read = dev_read,
  .write = dev_write,
  .open = dev_open,
  .owner = THIS_MODULE,

  /*Is effectively close, however as a file may be opened multople times,
    it features different naming convention */
  .release = dev_close,
};


static int __init charDriver_init(void){
 printk(KERN_INFO "charDriver: Initializing the charDriver LKM.\n");

 //Dynamic major number allocation
  majorNumber =  register_chrdev(0, DEVICE_NAME, &fops);
  if (majorNumber < 0){
    printk(KERN_ALERT "charDriver: Unable to allocate major number.\n");
    return majorNumber;    
  }

  //Associate a mutex with the device. Mode 1 is unlocked.
  sema_init(&myDevice.sem, 1);

  //Initialize cdev (a kind of driver driver) with the device.
  cdev_init(&myDevice.cdev, &fops);
  myDevice.cdev.owner = THIS_MODULE;

  /*Add the device to cdev, set to respond to this modules' major number, 
    with 1 device associated. */
  cdev_add(&myDevice.cdev, MKDEV(majorNumber, 0), 1);

  printk(KERN_INFO "charDriver: Device allocated major number.\n");

  //Create and register the driver class.
  charDriverClass = class_create(THIS_MODULE, CLASS_NAME);
  if (IS_ERR(charDriverClass)){
    unregister_chrdev(majorNumber, DEVICE_NAME);
    cdev_del(&myDevice.cdev);
    printk(KERN_ALERT "Failed to create class.\n");
    return PTR_ERR(charDriverClass);
  }

  //Create and register driver device.
  charDriverDevice = device_create(charDriverClass, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
  if (IS_ERR(charDriverDevice)){
    class_destroy(charDriverClass);
    unregister_chrdev(majorNumber, DEVICE_NAME);
    printk(KERN_ALERT "Failed to create device. \n");
    return PTR_ERR(charDriverDevice);
  }
  printk(KERN_INFO "charDriver: Device Class was created. \n");
  return 0;
}

static ssize_t dev_read (struct file* filep, char* buffer, size_t len, loff_t* offset){
  int errCount = 0;

  //Copy into the buffer, from @data, a block of @data size data
  errCount = copy_to_user(buffer, myDevice.data, myDevice.size_of_data);

  if (errCount == 0){
    printk(KERN_INFO "charDriver: %d characters sent to user. \n", myDevice.size_of_data);
    return (myDevice.size_of_data = 0);
  }
  else{
    printk(KERN_INFO "charDriver: Unable to send %d characters to user.\n", myDevice.size_of_data);
    return -EFAULT;
  }
}

static ssize_t dev_write (struct file* filep, const char* buffer, size_t len, loff_t* offset){
  //Read a string from the buffer (from user) into data.
  sprintf(myDevice.data, "%s", buffer);
  myDevice.size_of_data = strlen(myDevice.data);
  printk(KERN_INFO "charDriver: %d recieved from user.\n", myDevice.size_of_data);
  return myDevice.size_of_data;
}

static int dev_open (struct inode *inodep, struct file* filep){
  //Lock module as in use. Allow interruptions.
  down_interruptible(&myDevice.sem);
  numberOpens++;
  printk(KERN_INFO "charDriver: Device opened %d time(s).\n", numberOpens);
  return 0;
}

static int dev_close (struct inode *inodep, struct file* filep){
  //Module now free for use.
  up(&myDevice.sem);
  printk(KERN_INFO "charDriver: Device closed. \n");
  return 0;
}

static void __exit charDriver_exit(void)
{
  //Cleanup.
  device_destroy(charDriverClass, MKDEV(majorNumber, 0));
  class_unregister(charDriverClass);
  class_destroy(charDriverClass);
  unregister_chrdev(majorNumber, DEVICE_NAME);
  printk(KERN_INFO "charDriver: Quitting... \n");
}

  module_init(charDriver_init);
  module_exit(charDriver_exit);