obj-m += charDriver.o
KVERSION = $(shell uname -r)
SOURCE = charDriver.c S3436413UserApplication.c
USER = S3436413
README = Readme.txt
MAKEFILE = Makefile

all:
	make -C /lib/modules/$(KVERSION)/build M=$(PWD) modules
	$(CC) S3436413UserApplication.c -Wall -pedantic -o S3436413UserApplication
clean:
	make -C /lib/modules/$(KVERSION)/build M=$(PWD) clean
	rm S3436413UserApplication

.PHONY:archive
archive:
	zip $(USER)-a1 $(SOURCE) $(README) $(MAKEFILE) 